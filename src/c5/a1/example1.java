package c5.a1;

public class example1 {
    public static void main(String[] args) {
        // 订单的年份月份毫秒值，定义一个数组即可
        int[] arr = {2019,0504,1101};
        //调用方法，用一个变量接受结果
        String s = arrayToString(arr);
        //输出结果
        System.out.println("s:" + s);
    }
    // 定义一个方法实现数组拼接成字符串。参数类型为数组 返回值类型为String
    public static String arrayToString(int[] arr) {
        String s = "";
        s += "[";
        for (int i = 0; i < arr.length; i++) {
            if(i == arr.length-1) {
                s+=arr[i];
            }else {
                s+= arr[i];
            }
        }
        s += "]";
        //在方法中将数组遍历，然后把每一个得到的字符拼接成一个字符串并且返回
        return s;
    }
}

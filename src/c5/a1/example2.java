package c5.a1;

import java.util.Scanner;

public class example2 {
    public static void main(String[] args) {
        //键盘输入一个字符串，用Scanner实现
        Scanner sc = new Scanner(System.in);
        System.out.println("请您输入用户名：");
        String line = sc.nextLine();
        //调用方法，用一个变量接收
        String reverse = reverse(line);
        //输出结果
        System.out.println("恭喜"+line+"用户注册成功,您的初始密码为:"+reverse);
    }
    //定义一个方法实现字符串反转
    public static String reverse(String s) {
        String ss = "";
        //在方法中将字符串倒着遍历，然后把每一个得到的字符拼接成一个字符串并且返回
        for (int i = s.length()-1; i >=0; i--) {
            ss+=s.charAt(i);
        }
        return ss;
    }
}

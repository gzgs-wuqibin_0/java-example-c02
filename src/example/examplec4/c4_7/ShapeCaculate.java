package example.examplec4.c4_7;

public class ShapeCaculate {
    // 可以计算任意shape子类的面积
    public void calArea (Shape shape) {
        System.out.println(shape.getArea());
    }
    // 可以计算任意shape子类的周长
    public void calPerimeter(Shape shape) {
        System.out.println(shape.getPerimeter());
    }
}

package example.examplec4.c4_7;

public class TestShape {
    public static void main(String[] args) {
        // 创建图形计算器
        ShapeCaculate sc = new ShapeCaculate();
        // 创建长方形和圆形对象
        Shape rectangle = new Rectangle(3, 4);         // <-------多态
        Circle circle = new Circle(3);
        // 求长方形和圆形的面积
        System.out.println("长方形的面积：");
        sc.calArea(rectangle);
        System.out.println("圆形的面积：");
        sc.calArea(circle);
        // 求长方形和圆形的周长
        System.out.println("长方形的周长：");
        sc.calPerimeter(rectangle);
        System.out.println("圆形的周长：");
        sc.calPerimeter(circle);
    }
}

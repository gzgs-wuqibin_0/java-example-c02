package example.examplec4.c4_7;

public class Rectangle extends Shape {
    private double length = 0;    // 长方形的长
    private double width = 0;     // 长方形的宽
    // 有参构造，初始化长方形的长和宽
    public Rectangle(double length, double width) {
        super();
        this.length = length;
        this.width = width;
    }
    public double getArea() {
        return this.length*this.width;
    }
    public double getPerimeter() {
        return 2*(this.length+this.width);
    }
}

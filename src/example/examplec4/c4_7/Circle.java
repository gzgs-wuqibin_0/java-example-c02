package example.examplec4.c4_7;

public class Circle extends Shape {
    private double radius = 0;    // 圆的半径
    private final static double PI = 3.14;    // 常量，圆周率
    // 有参构造，初始化圆半径
    public Circle(double radius) {
        this.radius = radius;
    }
    // 求圆面积
    public double getArea() {
        return PI*radius*radius;
    }
    // 求圆周长
    public double getPerimeter() {
        return 2*radius*PI;
    }
}


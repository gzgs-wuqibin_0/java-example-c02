package example.examplec4.c4_7;

public abstract class Shape {
    // 抽象方法： 获取面积
    public abstract double getArea();
    // 抽象方法：获取周长
    public abstract double getPerimeter();
}

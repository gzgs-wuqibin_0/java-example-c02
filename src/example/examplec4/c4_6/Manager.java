package example.examplec4.c4_6;

public class Manager extends Employee{
    private String level = "";
    public Manager(String name, String address, String ID, double wage,
                   int age, String level){
        super(name, address, ID, wage, age);
        this.level = level;
    }
    //实现抽象方法
    public void add(){
        double wage = super.getWage();
        super.setWage(wage*1.1);
    }
    public void add(String position){
        double wage = super.getWage();
        super.setWage(wage*1.2);
    }
    public String getLevel() {
        return level;
    }
    public void setLevel(String level) {
        this.level = level;
    }
}

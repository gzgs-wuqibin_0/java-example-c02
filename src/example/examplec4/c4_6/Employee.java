package example.examplec4.c4_6;

public abstract class  Employee extends Person {
    private String ID = "";
    private double wage = 0;
    private int age = 0;
    public Employee(String name, String address, String ID, double
            wage, int age){
        super(name, address);
        this.ID = ID;
        this.wage = wage;
        this.age = age;
    }
    //定义抽象方法
    public abstract void add(String position);
    //设置get/set方法
    public double getWage() {
        return wage;
    }
    public void setWage(double wage) {
        this.wage = wage;
    }
}


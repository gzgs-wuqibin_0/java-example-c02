package example.examplec4.c4_6;

public class Test {
    public static void main(String[] args) {
        Manager normal = new Manager("wsl", "jit", "12", 1000, 2, "1");
        Manager manager = new Manager("ctl", "jitt", "123", 10000, 10,"0");
        normal.add();
        manager.add(manager.getLevel());
        System.out.println("normal wage is:"+normal.getWage());
        System.out.println("manager wage is:"+manager.getWage());
    }
}

package example.examplec4.c4_5;

public class TestDemo {
    public static void main(String[] args) {
        Graduate gr = new Graduate("zhangsan", "男", 5, 8000, 3000);
        judgeLoan(gr);
    }
    public static void judgeLoan(Graduate gr) {//对象作形参
        if (gr.getPay() * 12 - gr.getFree() * 2 < 2000) {
            System.out.println("provide a loan");
        } else
            System.out.println("don't need a loan");
    }
}


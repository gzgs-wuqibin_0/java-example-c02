package c6;

import java.util.TreeSet;
class Student12 implements Comparable<Student12> {
    private String id;
    private String name;
    public Student12(String id, String name) {
        this.id = id;
        this.name = name;
    }
    // 重写toString()方法
    public String toString() {
        return id + ":" + name;
    }
    @Override
    public int compareTo(Student12 o) {
        // return 0;          //集合中只有一个元素
         //return 1;          //集合按照怎么存就怎么取
           return -1;         //集合按照存入顺序倒过来进行存储
    }
}
public  class Example12 {
    public static void main(String[] args) {
         TreeSet ts = new TreeSet();
         ts.add(new Student12("1","张三"));
         ts.add(new Student12("2", "李四"));
         ts.add(new Student12("2", "王五"));
         System.out.println(ts);
        }
}

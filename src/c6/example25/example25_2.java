package c6.example25;

public class example25_2 {
    public static void main(String[] args) {
        Inter<String> inter = new InterImpl2();
        inter.show("hello");
        Inter<Integer> ii = new InterImpl2<>();
        ii.show(12);
    }
}

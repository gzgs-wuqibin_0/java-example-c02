package c6.example25;

public interface Inter<T> {
        public abstract void show(T t);
}

package c6;

import java.util.*;
class Student3 {
	String id;
	String name;		    		 
	public Student3(String id,String name) {	     // 创建构造方法
		this.id=id;
		this.name = name;
	}
	public String toString() {			              // 重写toString()方法
		return id+":"+name;
	}
}
public class Example08 {
	public static void main(String[] args) {
		HashSet hs = new HashSet();		              // 创建HashSet集合
		Student3 stu1 = new Student3("1", "张三");    // 创建Student对象
		Student3 stu2 = new Student3("2", "李四");
		Student3 stu3 = new Student3("2", "李四");
		hs.add(stu1);
		hs.add(stu2);
		hs.add(stu3);
		System.out.println(hs);
	}
}

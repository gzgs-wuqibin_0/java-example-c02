package c4.c4_1;

public class MyPointTest {
    public static void myShow(MyPoint a){
        a.show();
    }
    public static void main(String[] args){
        MyPoint mp1 = new MyPrintSquare();
        MyPoint mp2 = new MyPrintCircle();
        myShow(mp1);
        myShow(mp2);
    }
}
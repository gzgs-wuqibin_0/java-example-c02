package c4.Example01;

// 定义Animal类
class Animal01 {
    private String name;         			// 定义name属性
    private int age;             			// 定义name属性
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
// 定义Dog类继承Animal类
class Dog01 extends Animal01 {
    //此处不写任何代码
}
// 定义测试类
 public class Example01 {
    public static void main(String[] args) {
       Dog01 dog = new Dog01();    // 创建一个Dog类的实例对象
       dog.setName("牧羊犬");   // 此时访问的方法时父类中的，子类中并没有定义
       dog.setAge(3);           // 此时访问的方法时父类中的，子类中并没有定义
       System.out.println("名称："+dog.getName()+",年龄："+dog.getAge());
    }
}

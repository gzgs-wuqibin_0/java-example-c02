package c4.Example31;

public class DivideByMinusException extends Exception{
    public DivideByMinusException(){
        super();//调用父类的空参构造方法
    }

    public DivideByMinusException(String message){
        super(message);//调用父类的有参构造方法
    }
}

package c4.Example02;

// 定义Animal类
class Animal02 {
    private String name;         		// 定义name属性
    private int age;             		// 定义name属性
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
// 定义Dog类继承Animal类
class Dog02 extends Animal02 {
    private String color; // 定义name属性
    public String getColor() {
    return color;
}
public void setColor(String color) {
    this.color = color;
}
}
// 定义测试类
 public class Example02 {
    public static void main(String[] args) {
       Dog02 dog = new Dog02();    // 创建一个Dog类的实例对象
      dog.setName("牧羊犬");   // 此时访问的方法时父类中的，子类中并没有定义
      dog.setAge(3);           // 此时访问的方法时父类中的，子类中并没有定义
      dog.setColor("黑色");
      System.out.println("名称："+dog.getName()+",年龄："+dog.getAge()+", 颜色："+dog.getColor());
    }
}

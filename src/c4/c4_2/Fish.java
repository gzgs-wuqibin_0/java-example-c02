package c4.c4_2;

public class Fish extends Animal implements Food{
    public Fish(String name) {
        super(name);
    }
    @Override
    public void shout() {
    }
    @Override
    public void eat(Food food) {
    }
}

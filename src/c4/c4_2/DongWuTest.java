package c4.c4_2;

public class DongWuTest {
    public static void main(String[] args) {
        Feeder feeder = new Feeder("小华");
        feeder.speak();
        Dog dog = new Dog("小狗");
        dog.shout();
        Food food = new Bone();
        feeder.feed(dog, food);
        Cat cat = new Cat("小猫");
        cat.shout();
        food = new Fish("黄花鱼");
        feeder.feed(cat, food);
    }
}


package c4.Example03;

// 定义Animal类
class Animal03 {
    //定义动物叫的方法		
	void shout() {			  
		System.out.println("动物发出叫声");
	}
}
// 定义Dog类继承动物类
class Dog03 extends Animal03 {
	//重写父类Animal中的shout()方法
	void shout() {			 
		System.out.println("汪汪汪……");  
	}
}
// 定义测试类
public class Example03 {	
	public static void main(String[] args) {
		Dog03 dog = new Dog03(); // 创建Dog类的实例对象
		dog.shout();           // 调用dog重写的shout()方法
	}
}
